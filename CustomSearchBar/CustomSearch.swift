//
//  CustomSearch.swift
//  CustomSearchBar
//
//  Created by praveen velanati on 1/28/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

class CustomSearch: UISearchBar {

    var preferredFont : UIFont!
    var preferredTextColor : UIColor!
    
    init(frame : CGRect, font : UIFont ,textColor : UIColor) {
        super.init(frame :frame)
        self.frame = frame
        preferredFont = font
        preferredTextColor = textColor
        
        searchBarStyle = UISearchBarStyle.Prominent
        translucent = false
        
        
    }

    
    required init?(coder aDecoder : NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func indexOfSearchFieldInSubViews() -> Int! {
        var index : Int!
        let searchBarView = subviews[0] as! UIView
        
        for var i = 0; i<searchBarView.subviews.count; ++i {
            
            if searchBarView.subviews[i].isKindOfClass(UITextField) {
                index = i
                break
            }
            
        }
       return index
    }
    
    
    override func drawRect(rect: CGRect) {
        // Find the index of the search field in the search bar subviews.
        if let index = indexOfSearchFieldInSubViews() {
             // Access the search field
            let searchField : UITextField = (subviews[0] as! UIView).subviews[index] as! UITextField
            
            
            // Set its frame.
            
            searchField.frame = CGRectMake(5.0, 5.0, frame.size.width - 10.0, frame.size.height - 10.0)
            
            //Set the font and text color of the search field.
            
            searchField.font = preferredFont
            searchField.backgroundColor = barTintColor
            
        }
        
        
        
        var startPoint = CGPointMake(0.0, frame.size.height)
        var endPoint = CGPointMake(frame.size.width, frame.size.height)
        var path = UIBezierPath()
        path.moveToPoint(startPoint)
        path.addLineToPoint(endPoint)
        
        var shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        shapeLayer.strokeColor = preferredTextColor.CGColor
        shapeLayer.lineWidth = 2.5
        layer.addSublayer(shapeLayer)
    
            super.drawRect(rect)
        
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

