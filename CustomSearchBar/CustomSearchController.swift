//
//  CustomSearchController.swift
//  CustomSearchBar
//
//  Created by praveen velanati on 1/28/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

protocol CustomSearchControllerDelegate {
    
    func didStartSearching()
    
    func didTapOnSearchButton()
    
    func didTapOnCancelButton()
    
    func didChangesSearchText(searchText : String)
    
}


class CustomSearchController: UISearchController,UISearchBarDelegate {
    
    var customDelegate : CustomSearchControllerDelegate!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var customSearchBar : CustomSearch!
    
    init(searchResultsController : UIViewController! , searchBarFrame : CGRect ,searchBarFont : UIFont,searchBarTextColor : UIColor ,searchBarTintColor : UIColor) {
        
        super.init(searchResultsController : searchResultsController)
        
        configureSearchBar(searchBarFrame, font : searchBarFont , textColor :searchBarTextColor ,bgColor : searchBarTintColor)
        
    }
    
    
    func configureSearchBar(frame :CGRect,font :UIFont,textColor :UIColor , bgColor : UIColor) {
        
        customSearchBar = CustomSearch(frame: frame, font: font, textColor: textColor)
        
        customSearchBar.barTintColor = bgColor
        customSearchBar.tintColor = textColor
        customSearchBar.showsBookmarkButton = false
        customSearchBar.showsCancelButton = true
        customSearchBar.delegate = self
        

        
        
    }
        
        
        
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        customDelegate.didStartSearching()
    }
        
        
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        customSearchBar.resignFirstResponder()
        
        customDelegate.didTapOnSearchButton()
    }
        
        
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        customSearchBar.resignFirstResponder()
        customDelegate.didTapOnCancelButton()
        
    }
        
        
        
    func searchBar(searchBar : UISearchBar , textDidChange searchText : String){
        
        customDelegate.didChangesSearchText(searchText)
        
    }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        
        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
