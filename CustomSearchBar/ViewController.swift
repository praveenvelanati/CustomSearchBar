//
//  ViewController.swift
//  CustomSearchBar
//
//  Created by praveen velanati on 1/27/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchResultsUpdating ,UISearchBarDelegate,CustomSearchControllerDelegate{

    var customSearchController : CustomSearchController!
    var dataArray = [String]()
    var filteredArray = [String]()
    var shouldShowSearchResults = false
    var searchController : UISearchController!
    
    
    
    @IBOutlet weak var tblSearchResults: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tblSearchResults.delegate = self
        tblSearchResults.dataSource = self
        loadListOfCountries()
       // configureSearchController()
        configureCustomSearchController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: UITableView Delegate and Datasource functions
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            
            return filteredArray.count
        } else{
        
        return dataArray.count
    }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("idCell", forIndexPath: indexPath)
        
        if shouldShowSearchResults {
            cell.textLabel!.text = filteredArray[indexPath.row]
        }else {
            cell.textLabel!.text = dataArray[indexPath.row]
        }
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    
    func loadListOfCountries(){
        // Specify the path to the countries list file.
        let pathToFile = NSBundle.mainBundle().pathForResource("countries", ofType: "txt")
        
        if let path  = pathToFile {
            
            
            let countriesString = try! NSString(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            
            guard let _:NSString = countriesString else
            {
                print("error")
                return
            }
            
            dataArray = countriesString.componentsSeparatedByString("\n")
            
            tblSearchResults.reloadData()
            
        
            
        }
    }
    
    func configureCustomSearchController() {
        
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRectMake(0.0, 0.0, tblSearchResults.frame.size.width,50.0 ), searchBarFont: UIFont(name: "Futura", size: 16.0)!, searchBarTextColor: UIColor.orangeColor(), searchBarTintColor: UIColor.yellowColor())
        customSearchController.customSearchBar.placeholder = " Search in this awesome bar...."
        tblSearchResults.tableHeaderView = customSearchController.customSearchBar
            customSearchController.customDelegate = self
    }
    
    

    // default process
    
    func configureSearchController(){
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "search here... "
       searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        tblSearchResults.tableHeaderView = searchController.searchBar
        
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        shouldShowSearchResults = true
        tblSearchResults.reloadData()
        
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tblSearchResults.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tblSearchResults.reloadData()
        }
        searchController.searchBar.resignFirstResponder()
        
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let  searchString = searchController.searchBar.text
        
        filteredArray = dataArray.filter({ (country) -> Bool in
            let countryText: NSString = country
            
            return (countryText.rangeOfString(searchString!, options: NSStringCompareOptions.AnchoredSearch).location) != NSNotFound
        })
        
        // Reload the tableview.
        tblSearchResults.reloadData()
        
    }
    // end of default process
    
    
    func didStartSearching() {
        shouldShowSearchResults = true
        tblSearchResults.reloadData()
    }
    
    func didTapOnSearchButton() {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
        tblSearchResults.reloadData()
    }
    }
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        tblSearchResults.reloadData()
    }
    
    
    
    func didChangesSearchText(searchText: String) {
        
        // Filter the data array and get only those countries that match the search text.
        
        filteredArray = dataArray.filter({(country) -> Bool in
            
            let countryText :NSString = country
            
            return (countryText.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
        })
            
                 
        
        tblSearchResults.reloadData()
    }
    
    
    }
    
    
    























    

